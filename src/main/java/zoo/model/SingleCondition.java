package zoo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class SingleCondition {
    private String properties;
    private String conditionType;
}
