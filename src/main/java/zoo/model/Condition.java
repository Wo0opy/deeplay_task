package zoo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Condition {
    private SingleCondition weight;
    private SingleCondition height;
    private SingleCondition foodType;
}
