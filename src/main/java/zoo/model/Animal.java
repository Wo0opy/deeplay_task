package zoo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import zoo.property.FoodType;
import zoo.property.Height;
import zoo.property.Weight;

@Data
@Getter
@AllArgsConstructor
public class Animal {
    private int id;
    private String name;
    private Weight weight;
    private Height height;
    private FoodType foodType;
}
