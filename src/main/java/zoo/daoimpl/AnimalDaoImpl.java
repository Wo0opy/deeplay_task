package zoo.daoimpl;

import zoo.dao.AnimalDao;
import zoo.dbms.DBMS;
import zoo.model.Condition;

import java.util.List;

public class AnimalDaoImpl implements AnimalDao {
    private DBMS dbms;

    public AnimalDaoImpl(DBMS dbms) {
        this.dbms = dbms;
    }

    @Override
    public int getAnimalCountWithConditions(List<Condition> conditions) {
        return dbms.getAnimalsCount(conditions);
    }
}
