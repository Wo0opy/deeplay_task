package zoo.controller;

import com.google.gson.Gson;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zoo.daoimpl.AnimalDaoImpl;
import zoo.dbms.DBMS;
import zoo.dto.request.AnimalsWithConditionsRequest;
import zoo.dto.response.AnimalCountResponse;
import zoo.service.AnimalService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Data
public class Controller {
    private DBMS dbms;
    private AnimalService animalService;
    private Gson gson = new Gson();
    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

    public Controller(String databasePath) {
        this.dbms = new DBMS(databasePath);
        this.animalService = new AnimalService(new AnimalDaoImpl(dbms));
    }

    public AnimalCountResponse getAnimalCountWithConditions(String requestFile) throws IOException {
        AnimalsWithConditionsRequest request;
        AnimalCountResponse response;
        try (BufferedReader reader = new BufferedReader(new FileReader(requestFile))) {
            request = gson.fromJson(reader.readLine(), AnimalsWithConditionsRequest.class);
            response = animalService.getAnimalCountWithConditions(request);
            LOGGER.info("\nController: Executing request from file: '{}' successfully completed. Count = {}", requestFile, response.getCount());
            return response;
        } catch (IOException e) {
            LOGGER.info("\nController: Executing request from file: '{}' failed cause = {}", requestFile, e.getMessage());
            throw e;
        }
    }

}
