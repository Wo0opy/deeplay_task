package zoo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import zoo.model.Condition;

import java.util.List;

@Data
@Getter
@AllArgsConstructor
public class AnimalCountResponse {
    private List<Condition> conditions;
    private int count;
}
