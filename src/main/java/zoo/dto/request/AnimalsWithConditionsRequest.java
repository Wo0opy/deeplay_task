package zoo.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import zoo.model.Condition;

import java.util.List;

@AllArgsConstructor
@Data
public class AnimalsWithConditionsRequest {
    List<Condition> conditions;
}
