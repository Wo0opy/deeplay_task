package zoo.dao;

import zoo.model.Condition;

import java.util.List;

public interface AnimalDao {

    int getAnimalCountWithConditions(List<Condition> conditions);
}
