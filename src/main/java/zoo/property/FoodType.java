package zoo.property;

public enum FoodType {
    OMNIVORE,
    HERBIVORE,
    CARNIVORE
}
