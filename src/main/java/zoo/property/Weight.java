package zoo.property;

public enum Weight {
    HEAVY,
    LIGHT,
    AVERAGE
}
