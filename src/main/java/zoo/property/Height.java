package zoo.property;

public enum Height {
    TALL,
    LOW,
    AVERAGE
}
