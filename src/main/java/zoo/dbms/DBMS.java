package zoo.dbms;

import com.google.gson.Gson;
import zoo.model.Animal;
import zoo.model.Condition;
import zoo.model.SingleCondition;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class DBMS {
    private String databasePath;
    private Gson gson = new Gson();

    public DBMS(String databasePath) {
        this.databasePath = databasePath;
    }

    public int getAnimalsCount(List<Condition> conditions) {
        int count = 0;
        Animal animal;
        try (BufferedReader reader = new BufferedReader(new FileReader(databasePath))) {
            while (reader.ready()) {
                animal = gson.fromJson(reader.readLine(), Animal.class);
                if (checkConditions(animal, conditions)) {
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    private boolean checkConditions(Animal animal, List<Condition> conditions) {
        boolean isSatisfies = false;
        for (Condition condition : conditions) {
            if (isSatisfies) {
                return true;
            }
            isSatisfies =
                    checkSingleCondition(condition.getWeight(), animal.getWeight().toString()) &&
                            checkSingleCondition(condition.getHeight(), animal.getHeight().toString()) &&
                            checkSingleCondition(condition.getFoodType(), animal.getFoodType().toString());
        }
        return isSatisfies;
    }

    private boolean checkSingleCondition(SingleCondition condition, String animalProperty) {
        boolean isSatisfies = true;
        if (condition != null) {
            switch (condition.getConditionType()) {
                case "CONDITION":
                    isSatisfies = condition.getProperties().contains(animalProperty);
                    break;
                case "NOT_CONDITION":
                    isSatisfies = !condition.getProperties().contains(animalProperty);
                    break;
            }
        }
        return isSatisfies;
    }
}
