package zoo.service;

import lombok.Data;
import zoo.dao.AnimalDao;
import zoo.dto.request.AnimalsWithConditionsRequest;
import zoo.dto.response.AnimalCountResponse;

@Data
public class AnimalService {
    private AnimalDao dao;

    public AnimalService(AnimalDao dao) {
        this.dao = dao;
    }

    public AnimalCountResponse getAnimalCountWithConditions(AnimalsWithConditionsRequest request) {
        int count = dao.getAnimalCountWithConditions(request.getConditions());
        return new AnimalCountResponse(request.getConditions(), count);
    }

}
