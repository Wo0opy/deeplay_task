package zoo.controller;


import com.google.gson.Gson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import zoo.dto.request.AnimalsWithConditionsRequest;
import zoo.model.Animal;
import zoo.model.Condition;
import zoo.model.SingleCondition;
import zoo.property.FoodType;
import zoo.property.Height;
import zoo.property.Weight;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ControllerTest {

    @BeforeEach
    void createDatabaseFile() {
        Animal elephant = new Animal(1, "elephant", Weight.HEAVY, Height.TALL, FoodType.HERBIVORE);
        Animal bear = new Animal(2, "bear", Weight.HEAVY, Height.TALL, FoodType.OMNIVORE);
        Animal mouse = new Animal(3, "mouse", Weight.LIGHT, Height.LOW, FoodType.HERBIVORE);
        Animal dog = new Animal(4, "dog", Weight.AVERAGE, Height.AVERAGE, FoodType.OMNIVORE);
        Animal wolf = new Animal(5, "dog", Weight.AVERAGE, Height.AVERAGE, FoodType.CARNIVORE);
        Animal snake = new Animal(6, "snake", Weight.LIGHT, Height.AVERAGE, FoodType.CARNIVORE);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("database.dat"))) {
            writer.write(new Gson().toJson(elephant) + System.getProperty("line.separator"));
            writer.write(new Gson().toJson(bear) + System.getProperty("line.separator"));
            writer.write(new Gson().toJson(mouse) + System.getProperty("line.separator"));
            writer.write(new Gson().toJson(dog) + System.getProperty("line.separator"));
            writer.write(new Gson().toJson(wolf) + System.getProperty("line.separator"));
            writer.write(new Gson().toJson(snake) + System.getProperty("line.separator"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeEach
    void createRequestFile() {
        List<Condition> conditions = new ArrayList<>();
        conditions.add(new Condition(null, null, new SingleCondition("HERBIVORE", "CONDITION")));
        AnimalsWithConditionsRequest request = new AnimalsWithConditionsRequest(conditions);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("request.dat"))) {
            writer.write(new Gson().toJson(request));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetAnimalCountWithConditions() throws IOException {
        Controller controller = new Controller("database.dat");
        assertEquals(controller.getAnimalCountWithConditions("request.dat").getCount(), 2);
    }

}
